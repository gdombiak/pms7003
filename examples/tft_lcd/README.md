
## Hardware

* 3.5 Inch 480x320 TFT Touch Screen LCD Display for Raspberry Pi. Links: [Amazon](https://www.amazon.com/Raspberry-Display-Elegoo-480x320-Interface/dp/B01N3JROH8) or [elegoo](https://www.elegoo.com/product/elegoo-3-5-inch-480x320-tft-touch-screen-monitor). [User Manual](https://www.elegoo.com/tutorial/Elegoo%203.5%20inch%20Touch%20Screen%20User%20Manual%20V1.00.2017.10.09.zip)
