## Hardware

* 0.96" I2C IIC Serial 128X64 OLED LCD 3.3V-5V SSD1306 [Adafruit similar](https://www.adafruit.com/product/931)
* Plantower PMS7003
* Raspberry Pi B+ v1.2 (Disabling serial port for RPi 3 might differ)

## Library

* [Adafruit_SSD1306](https://github.com/adafruit/Adafruit_Python_SSD1306)

## Assembly

OLED module works with 3.3V and 5V so you can choose whatever is most convenient to you.

Connect the following way:

1. GND to GND
1. VCC to 3V
1. SCL to SCL1 (GPIO3)
1. SDA to SDA1 (GPIO2)

## Setup

* Enable i2c from raspi-config
* Execute following commands

1. sudo apt-get install libopenjp2-7
1. sudo apt install libtiff5
1. sudo apt-get install ttf-mscorefonts-installer

#### Old info below
```
sudo apt-get update
sudo apt-get install build-essential python-dev python-pip
sudo pip install RPi.GPIO
sudo apt-get install python-imaging python-smbus
```

```
sudo apt-get install git
git clone https://github.com/adafruit/Adafruit_Python_SSD1306.git
cd Adafruit_Python_SSD1306
sudo python setup.py install
```
