#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import traceback
import serial
import datetime
import time
import struct

import Adafruit_SSD1306

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

# Make sure /boot/cmdline.txt contains console=serial0,115200

# sudo echo 23 > /sys/class/gpio/export
# sudo echo out >/sys/class/gpio/gpio23/direction
PMS7003_RESET_GPIO = '/sys/class/gpio/gpio23'  # GPIO wired to RESET line of PMS7003, must be already exported and set to output.

# Attempt a sensor reset after some errors.
RESET_ON_FRAME_ERRORS = 2
# Abort the averaged read after too many errors.
MAX_FRAME_ERRORS = 10

# Normal data frame length.
DATA_FRAME_LENGTH = 28
# Command response frame length.
CMD_FRAME_LENGTH = 4
# Total Response Time is 10 seconds (sensor specifications).
MAX_TOTAL_RESPONSE_TIME = 12
# Serial read timeout value in seconds.
SERIAL_TIMEOUT = 2.0

# Use ttyS0 for RPi3. Older version used to use ttyAMA0
port = serial.Serial('/dev/ttyS0', baudrate=9600, timeout=SERIAL_TIMEOUT)



# Raspberry Pi pin configuration:
RST = None     # on the PiOLED this pin isnt used

# 128x32 display with hardware I2C:
# disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)

# 128x64 display with hardware I2C:
disp = Adafruit_SSD1306.SSD1306_128_64(rst=RST)

# Initialize library.
disp.begin()

# Clear display.
disp.clear()
disp.display()

# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
width = disp.width
height = disp.height
image = Image.new('1', (width, height))

# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)

# Draw a black filled box to clear the image.
draw.rectangle((0,0,width,height), outline=0, fill=0)

# Draw some shapes.
# First define some constants to allow easy resizing of shapes.
padding = -2
top = padding
bottom = height-padding
# Move left to right keeping track of the current x position for drawing shapes.
x = 0

# Use 12 size font when rendering all 6 rows
#font = ImageFont.truetype("fonts/truetype/msttcorefonts/Courier_New.ttf", 12)
# Use 20 size fonr when rendering only 3 rows
font_14pt = ImageFont.truetype("fonts/truetype/msttcorefonts/Courier_New.ttf", 12)
font_20pt = ImageFont.truetype("fonts/truetype/msttcorefonts/Courier_New.ttf", 20)
font_48pt = ImageFont.truetype("fonts/truetype/msttcorefonts/Courier_New.ttf", 48)

end_program = False
average_pm2_5 = -1

#---------------------------------------------------------------
# Convert a two bytes string into a 16 bit integer.
#---------------------------------------------------------------
def int16bit(b):
    return (b[0] << 8) + b[1]

#---------------------------------------------------------------
# Return the hex dump of a buffer of bytes.
#---------------------------------------------------------------
def buff2hex(b):
    return " ".join("0x{:02x}".format(c) for c in b)

#---------------------------------------------------------------
# Send a RESET signal to sensor.
#---------------------------------------------------------------
def sensor_reset():
#    print("Sending RESET signal to sensor")
    try:
        gpio_direction = None
        if not os.path.isdir(PMS7003_RESET_GPIO):
            print(u'GPIO for sensor RESET is not exported: %s' % (PMS7003_RESET_GPIO,))
            return
        else:
            with open(os.path.join(PMS7003_RESET_GPIO, 'direction'), 'r') as f:
                gpio_direction = f.read().strip()
        if gpio_direction != 'out':
            print(u'GPIO for sensor RESET is not set to output')
            return
        else:
            print(u'Setting GPIO line to LOW for a short time')
            with open(os.path.join(PMS7003_RESET_GPIO, 'value'), 'w') as f:
                f.write("0\n")
            time.sleep(0.5)
            with open(os.path.join(PMS7003_RESET_GPIO, 'value'), 'w') as f:
                f.write("1\n")
            time.sleep(1.0)
    except Exception as e:
        print(u'PMS7003 sensor RESET via GPIO line: Exception %s' % (str(e),))
    except KeyboardInterrupt:
        sys.exit(1)
    return

#---------------------------------------------------------------
# Read a data frame from serial port, the first 4 bytes are:
# 0x42, 0x4d, frame lenght (16 bit integer).
# Return None on errors.
#---------------------------------------------------------------
def read_pm_frame(_port):
    frame = b''
    start = datetime.datetime.utcnow()
    while True:
        b0 = _port.read()
        if b0 == '':
            print('Timeout on serial read()')
#        else:
#            print('Got char 0x%x from serial read()' % (b0,))
        if b0 == b'\x42':
            b1 = _port.read()
            if b1 == b'\x4d':
                b2 = _port.read()
                b3 = _port.read()
                frame_len = ord(b2) * 256 + ord(b3)
                if frame_len == DATA_FRAME_LENGTH:
                    # Normal data frame.
                    frame += b0 + b1 + b2 + b3
                    frame += _port.read(frame_len)
                    if (len(frame) - 4) != frame_len:
                        print("Short read, expected %d bytes, got %d" % (frame_len, len(frame) - 4))
                        return None
                    # Verify checksum (last two bytes).
                    expected = int16bit(frame[-2:])
                    checksum = 0
                    for i in range(0, len(frame) - 2):
                        checksum += frame[i]
                    if checksum != expected:
                        print("Checksum mismatch: %d, expected %d" % (checksum, expected))
                        return None
#                    print("Received data frame = %s" % (buff2hex(frame),))
                    return frame
                elif frame_len == CMD_FRAME_LENGTH:
                    # Command response frame.
                    frame += b0 + b1 + b2 + b3
                    frame += _port.read(frame_len)
#                    print("Received command response frame = %s" % (buff2hex(frame),))
                    return frame
                else:
                    # Unexpected frame.
                    print("Unexpected frame length = %d" % (frame_len))
                    time.sleep(MAX_TOTAL_RESPONSE_TIME)
                    _port.flushInput()
                    return None

        if (datetime.datetime.utcnow() - start).seconds >= MAX_TOTAL_RESPONSE_TIME:
            print("Timeout waiting data-frame signature")
            return None


#---------------------------------------------------------------
# Command functions
#---------------------------------------------------------------
def build_cmd(mode):
    '''
    construct a custom command and sent to the serial port
    https://github.com/teusH/MySense/blob/master/PyCom/lib/PMSx003.py
    send 42 4D cmd(E2, E4, E1) 00 ON(On=1, Off=0) chckH chckL
    no answer on cmd: E2 (read telegram) and E4 On (active mode)
    answer 42 4D 00 04 cmd 00 chckH chckL
    '''
    d = [0x42, 0x4D]
    if mode == 'sleep':
        cmd = [0xE4, 0x00, 0x00]
    elif mode == 'wakeup':
        cmd = [0xE4, 0x00, 0x01]
    elif mode == 'active':
        cmd = [0xE1, 0x00, 0x01]
    elif mode == 'passive':
        cmd = [0XE1, 0x00, 0x00]
    elif mode == 'read_passive':
        cmd = [0xE2, 0x00, 0x00]
    else:
        print('No mode selected')
        return
    d += cmd
    ckcSum = sum(x for x in d)
    d += [ckcSum]
    cmd = struct.pack('!BBBBBH', d[0], d[1], d[2], d[3], d[4], d[5])
    return cmd


def send_cmd(_port, cmd):
    '''send command to the sensor'''
    try:
        _port.reset_output_buffer()
        time.sleep(0.1)
        _port.write(cmd)
        _port.flush()
    except Exception as e:
        print(u'Exception sending command %s' % (str(e),))
    return None


#---------------------------------------------------------------
# Return the data frame in a verbose format.
#---------------------------------------------------------------
def draw_data_frame(f):
#    draw.text((x, top),       "PM1.0 μg/m³: " + str(f['data1']),  font=font, fill=255)
#    draw.text((x, top+11),     "PM2.5 μg/m³: " + str(f['data2']), font=font, fill=255)
#    draw.text((x, top+22),    "PM10  μg/m³: " + str(f['data3']),  font=font, fill=255)
#    draw.text((x, top+33),    "PM1.0 μg/m³: " + str(f['data4']),  font=font, fill=255)
#    draw.text((x, top+44),    "PM2.5 μg/m³: " + str(f['data5']),  font=font, fill=255)
#    draw.text((x, top+55),    "PM10  μg/m³: " + str(f['data6']),  font=font, fill=255)
    draw.text((x, top),       "PM1.0: " + str(f['data1']), font=font_20pt, fill=255)
    draw.text((x, top+22),     "PM2.5: " + str(f['data2']), font=font_20pt, fill=255)
    draw.text((x, top+44),    "PM10: " + str(f['data3']), font=font_20pt, fill=255)

def read_and_display(seconds):
    start_time = time.time()
    current_time = time.time()
    error_count = 0
    error_total = 0
    read_count = 0
    total_read = 0
    while current_time - start_time <= seconds:
        try:
            rcv = read_pm_frame(port)

            # Manage data-frame errors.
            if rcv == None:
                error_count += 1
                error_total += 1
                if error_count >= RESET_ON_FRAME_ERRORS:
                    print("Repeated read errors, attempting sensor reset")
                    sensor_reset()
                    error_count = 0
                    continue
                if error_total >= MAX_FRAME_ERRORS:
                    print("Too many read errors, exiting")
                    break

            # Skip non-output data-frames.
            if (rcv == None) or ((len(rcv) - 4) != DATA_FRAME_LENGTH):
                continue

            # Got a valid data-frame.
            res = {'timestamp': datetime.datetime.utcnow(),
                   'data1': int16bit(rcv[4:]),
                   'data2': int16bit(rcv[6:]),
                   'data3': int16bit(rcv[8:]),
                   'data4': int16bit(rcv[10:]),
                   'data5': int16bit(rcv[12:]),
                   'data6': int16bit(rcv[14:]),
                   'data7': int16bit(rcv[16:]),
                   'data8': int16bit(rcv[18:]),
                   'data9': int16bit(rcv[20:]),
                   'data10': int16bit(rcv[22:]),
                   'data11': int16bit(rcv[24:]),
                   'data12': int16bit(rcv[26:]),
                   'reserved': buff2hex(rcv[28:30]),
                   'checksum': int16bit(rcv[30:])
                   }

            # Update states to calculate average
            total_read += res['data2']
            read_count += 1

            # Draw a black filled box to clear the image.
            draw.rectangle((0, 0, width, height), outline=0, fill=0)

            draw_data_frame(res)
            # Display image.
            disp.image(image)
            disp.display()
            time.sleep(.1)

            current_time = time.time()
        except KeyboardInterrupt:
            break
        except:
            print(traceback.format_exc())
    # Remember last read value
    global average_pm2_5
    average_pm2_5 = round(total_read / read_count)

def countdown_display(text, seconds):
    start_time = time.time()
    current_time = time.time()
    while current_time - start_time <= seconds:
        elapsed = current_time - start_time
        # Use last read value
        global average_pm2_5

        # Draw a black filled box to clear the image.
        draw.rectangle((0, 0, width, height), outline=0, fill=0)

        draw.text((x, top + 12), "PM2.5: ", font=font_14pt, fill=255)
        draw.text((round(width / 2) - 20, top), str(average_pm2_5), font=font_48pt, fill=255)
        draw.text((x, height - 14), text + " " + str(round(seconds - elapsed)), font=font_14pt, fill=255)

        # Display image.
        disp.image(image)
        disp.display()
        time.sleep(1)

        current_time = time.time()

def wakeup_and_display():
    send_cmd(port, build_cmd('wakeup'))
    countdown_display('WAKING UP', 30)

def sleep_and_display(seconds):
    # Ask PMS7003 to sleep (saves power and sensor life)
    send_cmd(port, build_cmd('sleep'))
    countdown_display('SLEEPING', seconds)

#---------------------------------------------------------------
# Main function
#---------------------------------------------------------------
while not end_program:
    try:
        wakeup_and_display()        # Wake up sensor and wait 30 seconds to stabilize
        read_and_display(30)        # Display numbers for 30 seconds
        sleep_and_display(60 * 15)  # Sleep 15 minutes
    except KeyboardInterrupt:
        # Clear display.
        disp.clear()
        disp.display()
        # End program
        end_program = True
        break
