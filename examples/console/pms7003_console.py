#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import traceback
import serial
import datetime
import time

# Make sure /boot/cmdline.txt contains console=serial0,115200

# sudo echo 23 > /sys/class/gpio/export
# sudo echo out >/sys/class/gpio/gpio23/direction
PMS7003_RESET_GPIO = '/sys/class/gpio/gpio23'  # GPIO wired to RESET line of PMS7003, must be already exported and set to output.

# Attempt a sensor reset after some errors.
RESET_ON_FRAME_ERRORS = 2
# Abort the averaged read after too many errors.
MAX_FRAME_ERRORS = 10

# Normal data frame length.
DATA_FRAME_LENGTH = 28
# Command response frame length.
CMD_FRAME_LENGTH = 4
# Total Response Time is 10 seconds (sensor specifications).
MAX_TOTAL_RESPONSE_TIME = 12
# Serial read timeout value in seconds.
SERIAL_TIMEOUT = 2.0

# Use ttyS0 for RPi3. Older version used to use ttyAMA0
port = serial.Serial('/dev/ttyS0', baudrate=9600, timeout=SERIAL_TIMEOUT)

#---------------------------------------------------------------
# Convert a two bytes string into a 16 bit integer.
#---------------------------------------------------------------
def int16bit(b):
    return (b[0] << 8) + b[1]

#---------------------------------------------------------------
# Return the hex dump of a buffer of bytes.
#---------------------------------------------------------------
def buff2hex(b):
    return " ".join("0x{:02x}".format(c) for c in b)

#---------------------------------------------------------------
# Send a RESET signal to sensor.
#---------------------------------------------------------------
def sensor_reset():
#    print("Sending RESET signal to sensor")
    try:
        gpio_direction = None
        if not os.path.isdir(PMS7003_RESET_GPIO):
            print(u'GPIO for sensor RESET is not exported: %s' % (PMS7003_RESET_GPIO,))
            return
        else:
            with open(os.path.join(PMS7003_RESET_GPIO, 'direction'), 'r') as f:
                gpio_direction = f.read().strip()
        if gpio_direction != 'out':
            print(u'GPIO for sensor RESET is not set to output')
            return
        else:
            print(u'Setting GPIO line to LOW for a short time')
            with open(os.path.join(PMS7003_RESET_GPIO, 'value'), 'w') as f:
                f.write("0\n")
            time.sleep(0.5)
            with open(os.path.join(PMS7003_RESET_GPIO, 'value'), 'w') as f:
                f.write("1\n")
            time.sleep(1.0)
    except Exception as e:
        print(u'PMS7003 sensor RESET via GPIO line: Exception %s' % (str(e),))
    except KeyboardInterrupt:
        sys.exit(1)
    return

#---------------------------------------------------------------
# Read a data frame from serial port, the first 4 bytes are:
# 0x42, 0x4d, frame lenght (16 bit integer).
# Return None on errors.
#---------------------------------------------------------------
def read_pm_frame(_port):
    frame = b''
    start = datetime.datetime.utcnow()
    while True:
        b0 = _port.read()
        if b0 == '':
            print('Timeout on serial read()')
#        else:
#            print('Got char 0x%x from serial read()' % (b0,))
        if b0 == b'\x42':
            b1 = _port.read()
            if b1 == b'\x4d':
                b2 = _port.read()
                b3 = _port.read()
                frame_len = ord(b2) * 256 + ord(b3)
                if frame_len == DATA_FRAME_LENGTH:
                    # Normal data frame.
                    frame += b0 + b1 + b2 + b3
                    frame += _port.read(frame_len)
                    if (len(frame) - 4) != frame_len:
                        print("Short read, expected %d bytes, got %d" % (frame_len, len(frame) - 4))
                        return None
                    # Verify checksum (last two bytes).
                    expected = int16bit(frame[-2:])
                    checksum = 0
                    for i in range(0, len(frame) - 2):
                        checksum += frame[i]
                    if checksum != expected:
                        print("Checksum mismatch: %d, expected %d" % (checksum, expected))
                        return None
#                    print("Received data frame = %s" % (buff2hex(frame),))
                    return frame
                elif frame_len == CMD_FRAME_LENGTH:
                    # Command response frame.
                    frame += b0 + b1 + b2 + b3
                    frame += _port.read(frame_len)
#                    print("Received command response frame = %s" % (buff2hex(frame),))
                    return frame
                else:
                    # Unexpected frame.
                    print("Unexpected frame length = %d" % (frame_len))
                    time.sleep(MAX_TOTAL_RESPONSE_TIME)
                    _port.flushInput()
                    return None

        if (datetime.datetime.utcnow() - start).seconds >= MAX_TOTAL_RESPONSE_TIME:
            print("Timeout waiting data-frame signature")
            return None

#---------------------------------------------------------------
# Return the data frame in a verbose format.
#---------------------------------------------------------------
def data_frame_verbose(f):
    return (' PM1.0 (CF=1) μg/m³: {};\n'
            ' PM2.5 (CF=1) μg/m³: {};\n'
            ' PM10  (CF=1) μg/m³: {};\n'
            ' PM1.0 (STD)  μg/m³: {};\n'
            ' PM2.5 (STD)  μg/m³: {};\n'
            ' PM10  (STD)  μg/m³: {};\n'
            ' Particles >  0.3 μm count: {};\n'
            ' Particles >  0.5 μm count: {};\n'
            ' Particles >  1.0 μm count: {};\n'
            ' Particles >  2.5 μm count: {};\n'
            ' Particles >  5.0 μm count: {};\n'
            ' Particles > 10.0 μm count: {};\n'
            ' Reserved: {};\n'
            ' Checksum: {};'.format(
                f['data1'],  f['data2'],  f['data3'],
                f['data4'],  f['data5'],  f['data6'],
                f['data7'],  f['data8'],  f['data9'],
                f['data10'], f['data11'], f['data12'],
                f['reserved'], f['checksum']))

error_count = 0
error_total = 0
while True:
    try:
        rcv = read_pm_frame(port)

        # Manage data-frame errors.
        if rcv == None:
            error_count += 1
            error_total += 1
            if error_count >= RESET_ON_FRAME_ERRORS:
                print("Repeated read errors, attempting sensor reset")
                sensor_reset()
                error_count = 0
                continue
            if error_total >= MAX_FRAME_ERRORS:
                print("Too many read errors, exiting")
                break

        # Skip non-output data-frames.
        if (rcv == None) or ((len(rcv) - 4) != DATA_FRAME_LENGTH):
            continue

        # Got a valid data-frame.
        res = {'timestamp': datetime.datetime.utcnow(),
               'data1':     int16bit(rcv[4:]),
               'data2':     int16bit(rcv[6:]),
               'data3':     int16bit(rcv[8:]),
               'data4':     int16bit(rcv[10:]),
               'data5':     int16bit(rcv[12:]),
               'data6':     int16bit(rcv[14:]),
               'data7':     int16bit(rcv[16:]),
               'data8':     int16bit(rcv[18:]),
               'data9':     int16bit(rcv[20:]),
               'data10':    int16bit(rcv[22:]),
               'data11':    int16bit(rcv[24:]),
               'data12':    int16bit(rcv[26:]),
               'reserved':  buff2hex(rcv[28:30]),
               'checksum':  int16bit(rcv[30:])
               }

        print(data_frame_verbose(res))

    except KeyboardInterrupt:
        break
    except:
        print(traceback.format_exc())
