This example will read from PMS7003 and display values to the console.

## Execute example

1. Follow initial instructions listed [here](../../README.md)
1. Copy pms7003_console.py to your raspberry pi
1. Run with example from within venv and execute the following command line
```
python pms7003_console.py
```
