## List of Examples

Code used in examples to interface with PMS7003 was copied from [AirPi](https://github.com/RigacciOrg/AirPi) and cleaned up to just illustrate how to interact with PMS7003. Each example shows how to use PMS7003 and combine it with different modules to fulfill different requirements.

* Display reading from PMS7003 to console. [See example.](console)
* Display reading from PMS7003 to 'OLED i2c LCD'. [See example.](oled_lcd)
* Read from PMS7003 and send data via Wifi.
* Read from PMS7003 and send data via Ethernet cable.
* Display reading from PMS7003 to '3.5 inch RPi Display'. [See example.](tft_lcd)
