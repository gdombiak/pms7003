# Plantower PMS7003 laser dust sensor on Raspberry Pi with Python

In this project you will learn how to setup your new PMS7003 with your Raspberry Pi
and run a simple example that displays number of particles in the air.

## Hardware

* Raspberry Pi 3B
* Plantower PMS7003

## Software

* Raspbian Buster
* Use Python 3 or later

## Setup
#### OS config
1. Disable serial login shell and enable serial interface
    1. sudo raspi-config
    1. Select 'Interfacing Options' option
    1. Select 'P6 Serial' option
        1. Disable _serial login_ shell 
        1. Enable _serial interface_
    1. Save and reboot when prompted
1. Confirm that '/boot/config.txt' has 'enable_uart=1'
1. Make sure /boot/cmdline.txt contains console=serial0,115200. Probably needs to be added and then reboot
1. Optional in case you want to use RESET pin. This will export GPIO23 and set it to output
    1. sudo echo 23 > /sys/class/gpio/export
    1. sudo echo out >/sys/class/gpio/gpio23/direction


#### Python config
1. Go to folder where you checked out git repo
1. python3 -m venv venv
1. source venv/bin/activate
1. python -m pip install --upgrade pip
1. python -m pip install --upgrade Pillow
1. python -m pip install --upgrade smbus2
1. python -m pip install --upgrade pyserial
1. python -m pip install --upgrade RPi.GPIO   --> Has an error installing on Mac
1. python -m pip install --upgrade Adafruit-SSD1306

## Assembly

[RPi GPIO Pinout](https://pinout.xyz/)

![GitHub Logo](IMG_1554.png)

We will connect from PMS7003:

* VCC to 5V on the RPi (red cable)
* GND to GND on the RPi (black cable)
* RXD to TXD0 on the RPi (yellow cable)
* TXD to RXD0 on the RPi (orange cable)

![GitHub Logo](IMG_1555.png)

* Use Physical Pin 2 (5V) for 5V (red cable). Or you can use Pin 4 if you prefer
* Use Physical Pin 6 (GND) for Ground (black cable)
* Use Physical Pin 8 (TXD0) for TXD from the RPi (yellow cable)
* Use Physical Pin 10 (RXD0) for RXD from the RPi (orange cable)
* Optional Use Physical Pin 16 (GPIO23) for Reset ping

## Examples

Different examples can be found in the [examples](examples) folder.
